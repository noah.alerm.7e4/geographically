package com.itb.geographically.data.models

import android.graphics.Bitmap
import com.google.android.gms.maps.model.LatLng
import java.util.*

data class MarkerGeo(
    val id: Long,
    var name: String,
    var coords: LatLng,
    var type: String,
    var description: String,
    var image: Bitmap?,
    var creationDate: Date,
    var tags: MutableList<String> = mutableListOf(),
    var isFavorite: Boolean = false
)
