package com.itb.geographically.utils

import android.app.Activity
import androidx.core.os.bundleOf
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.Navigation
import com.itb.geographically.R
import com.itb.geographically.ui.viewModel.GeographicallyViewModel
import com.google.android.material.navigation.NavigationView

object NavigationDrawerOptions {
    //METHODS
    /**
     * This method is used to manage the navigation drawer's options.
     * @param drawerLayout The Navigation Drawer's DrawerLayout
     * @param navigationView The Navigation Drawer's NavigationView
     * @param activity Activity used for the navigation
     */
    fun manageNavigationOptions(drawerLayout: DrawerLayout, navigationView: NavigationView, activity: Activity, viewModel: GeographicallyViewModel) {
        //NAVIGATION DRAWER
        drawerLayout.openDrawer(GravityCompat.START)

        //VIEW MODEL UPDATE (Orientation Change Persistence)
        viewModel.newUsername = ""
        viewModel.bitmap = null
        viewModel.bitmapChanged = false

        //NAVIGATION DRAWER OPTIONS
        navigationView.setNavigationItemSelectedListener { menuItem ->
            // Handle menu item selected
            when (menuItem.itemId) {
                R.id.mapFragment -> Navigation.findNavController(activity, R.id.nav_host_fragment).navigate(R.id.mapFragment)
                R.id.listFragment -> Navigation.findNavController(activity, R.id.nav_host_fragment).navigate(R.id.markerListFragment, bundleOf("isFavorites" to false))
                R.id.favorites -> Navigation.findNavController(activity, R.id.nav_host_fragment).navigate(R.id.markerListFragment, bundleOf("isFavorites" to true))
            }

            drawerLayout.close()

            true
        }
    }
}
