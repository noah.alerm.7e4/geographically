package com.itb.geographically.ui.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.itb.geographically.R
import com.itb.geographically.data.models.MarkerGeo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class TagAdapter(private var markerTags: MutableList<String>, private var marker: MarkerGeo) : RecyclerView.Adapter<TagAdapter.TagViewHolder>() {
    //ADAPTER METHODS
    /**
     * This method is used to create a View Holder and to set up it's view.
     * @param parent Adapter's parent (used to get context)
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TagViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.tag_item, parent, false)

        return TagViewHolder(view)
    }

    /**
     * This method is used to set up the data of each item.
     * @param holder View Holder
     * @param position Current item
     */
    override fun onBindViewHolder(holder: TagViewHolder, position: Int) {
        holder.bindData(markerTags[position])

        //ANIMATION
        holder.itemView.animation = AnimationUtils.loadAnimation(holder.itemView.context, R.anim.list_animation)

        //TAG DELETION
        val deleteTagIcon: ImageView = holder.itemView.findViewById(R.id.delete_tag_icon)

        //ON CLICK
        deleteTagIcon.setOnClickListener {
            //LOCAL DELETE
            markerTags.removeAt(position)

            //FIRESTORE UPDATE / TAG DELETION
            Firebase.firestore.collection("users").document(FirebaseAuth.getInstance().currentUser!!.email!!)
                .collection("markers").document(marker.id.toString()).update(mapOf("tags" to marker.tags.toList()))

            //Recycler view update
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, markerTags.size)
        }
    }

    /**
     * This method is used to get the total amount of items in the Recycler View.
     */
    override fun getItemCount(): Int {
        return markerTags.size
    }


    //VIEW HOLDER
    class TagViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //ATTRIBUTES
        private var tagName: TextView = itemView.findViewById(R.id.tag_name)

        //METHODS
        /**
         * This method is used to set up the data of each item of the tag list.
         */
        @SuppressLint("SetTextI18n")
        fun bindData(tag: String) {
            tagName.text = tag
        }
    }
}
