package com.itb.geographically.ui.adapters

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.itb.geographically.R
import com.itb.geographically.data.models.MarkerGeo
import com.itb.geographically.ui.viewModel.GeographicallyViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage

class ListAdapter(private val markers: List<MarkerGeo>, private val loadingPanel: RelativeLayout, private val viewModel: GeographicallyViewModel)
    : RecyclerView.Adapter<ListAdapter.ListViewHolder>() {
    //ADAPTER METHODS
    /**
     * This method is used to create a View Holder and to set up it's view.
     * @param parent Adapter's parent (used to get context)
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item, parent, false)

        return ListViewHolder(view)
    }

    /**
     * This method is used to set up the data of each item.
     * @param holder View Holder
     * @param position Current item
     */
    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bindData(markers[position], loadingPanel, viewModel)

        //ANIMATION
        holder.itemView.animation = AnimationUtils.loadAnimation(holder.itemView.context, R.anim.list_animation)

        //ON CLICK
        holder.itemView.setOnClickListener {
            //NAVIGATION
            Navigation.findNavController(holder.itemView).navigate(R.id.action_markerList_to_detail, bundleOf("id" to markers[position].id))
        }
    }

    /**
     * This method is used to get the total amount of items in the Recycler View.
     */
    override fun getItemCount(): Int {
        return markers.size
    }


    //VIEW HOLDER
    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //ATTRIBUTES
        private var image: ImageView = itemView.findViewById(R.id.location_image)
        private var name: TextView = itemView.findViewById(R.id.name)
        private var favoritesIcon: ImageView = itemView.findViewById(R.id.favorites_icon)

        //METHODS
        /**
         * This method is used to set up the data of each item of the marker list.
         */
        @SuppressLint("SetTextI18n")
        fun bindData(marker: MarkerGeo, loadingPanel: RelativeLayout, viewModel: GeographicallyViewModel) {
            if (!viewModel.firstTimeInList) {
                if (marker.image != null)
                    image.setImageBitmap(marker.image)
            }
            else {
                //LOADING PANEL
                loadingPanel.visibility = View.VISIBLE

                var imageUri: Uri? = null

                FirebaseStorage.getInstance().getReferenceFromUrl("gs://geographically-341418.appspot.com/")
                    .child("images/" + FirebaseAuth.getInstance().currentUser!!.email!! + "/" + marker.id)
                    .downloadUrl.addOnSuccessListener {
                        imageUri = it
                        Log.d("URI", imageUri.toString())
                    }

                Handler(Looper.getMainLooper()).postDelayed({
                    if (imageUri != null) {
                        Glide.with(name.context).asBitmap().load(imageUri).into(object : CustomTarget<Bitmap>() {
                            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                image.setImageBitmap(resource)
                                marker.image = resource
                            }

                            override fun onLoadCleared(placeholder: Drawable?) {
                            }
                        })
                    }

                    //LOADING PANEL
                    loadingPanel.visibility = View.GONE
                }, 1000)
            }

            if (marker.name.length > 15)
                name.text = "${marker.name.subSequence(0, 12)}..."
            else
                name.text = marker.name

            setFavoritesImage(marker.isFavorite)

            //ON CLICK
            favoritesIcon.setOnClickListener {
                marker.isFavorite = !marker.isFavorite

                //FIRESTORE UPDATE
                Firebase.firestore.collection("users").document(FirebaseAuth.getInstance().currentUser!!.email!!)
                    .collection("markers").document(marker.id.toString()).update(mapOf("isFavorite" to marker.isFavorite))

                setFavoritesImage(marker.isFavorite)
            }
        }

        /**
         * This method is used to set up the favorites image.
         */
        private fun setFavoritesImage(isFavorite: Boolean) {
            //ICON ANIMATION
            YoYo.with(Techniques.Pulse).duration(400).playOn(favoritesIcon)

            if (isFavorite)
                favoritesIcon.setImageResource(R.drawable.favorites_icon)
            else
                favoritesIcon.setImageResource(R.drawable.no_favorites_icon)
        }
    }
}
