package com.itb.geographically.ui.views

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.core.os.bundleOf
import androidx.core.view.setPadding
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.itb.geographically.R
import com.itb.geographically.ui.viewModel.GeographicallyViewModel
import com.itb.geographically.utils.CameraAndGalleryManagement
import com.itb.geographically.utils.DialogCustomizer
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.storage.FirebaseStorage
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class UserFragment : Fragment(R.layout.fragment_user){
    //ATTRIBUTES
    private lateinit var loadingPanel: RelativeLayout
    private lateinit var closingIcon: ImageView
    private lateinit var editUserIcon: ImageView
    private lateinit var userIcon: ImageView
    private lateinit var rotationIcon: ImageView
    private lateinit var deleteImageButton: ImageView
    private lateinit var cameraIcon: ImageView
    private lateinit var galleryIcon: ImageView
    private lateinit var username: TextView
    private lateinit var editUsernameIcon: ImageView
    private lateinit var changePasswordButton: Button
    private lateinit var email: TextView
    private lateinit var joiningDate: TextView
    private lateinit var deleteUserButton: AppCompatButton
    private lateinit var saveChangesButton: Button
    private lateinit var cancelChangesButton: Button
    private lateinit var logoutText: TextView
    private lateinit var logoutIcon: ImageView

    //Editable Variables
    private var newUsername = ""
    private var currentBitmap: Bitmap? = null
    private var editedBitmap: Bitmap? = null

    //View Model
    private val viewModel: GeographicallyViewModel by activityViewModels()

    //ON VIEW CREATED
    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //ENTER ANIMATION
        view.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.fragment_enter_animation))

        //IDs
        loadingPanel = view.findViewById(R.id.loading_panel)
        closingIcon = view.findViewById(R.id.close_button)
        editUserIcon = view.findViewById(R.id.edit_user_button)
        userIcon = view.findViewById(R.id.user_icon)
        rotationIcon = view.findViewById(R.id.rotation_icon)
        deleteImageButton = view.findViewById(R.id.delete_image_button)
        cameraIcon = view.findViewById(R.id.camera_icon)
        galleryIcon = view.findViewById(R.id.gallery_icon)
        username = view.findViewById(R.id.username)
        editUsernameIcon = view.findViewById(R.id.edit_username)
        changePasswordButton = view.findViewById(R.id.update_password_button)
        email = view.findViewById(R.id.email)
        joiningDate = view.findViewById(R.id.joining_date)
        deleteUserButton = view.findViewById(R.id.delete_button)
        saveChangesButton = view.findViewById(R.id.save_button)
        cancelChangesButton = view.findViewById(R.id.cancel_button)
        logoutText = view.findViewById(R.id.logout_text)
        logoutIcon = view.findViewById(R.id.logout_icon)

        //DATA SET UP
        setUpUserData()

        //ON CLICK
        //Closing Icon
        closingIcon.setOnClickListener {
            //EXIT ANIMATION
            view.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.fragment_exit_animation))

            //VIEW MODEL UPDATE
            viewModel.editMode = false

            //NAVIGATION
            when (viewModel.currentFragment) {
                "Map" -> findNavController().navigate(R.id.action_user_to_map)
                "List" -> findNavController().navigate(R.id.action_user_to_markerList, bundleOf("isFavorites" to false))
                "Favorites" -> findNavController().navigate(R.id.action_user_to_markerList, bundleOf("isFavorites" to true))
            }
        }

        //Edit User Icon
        editUserIcon.setOnClickListener {
            setUpLayout(true)
            viewModel.editMode = true
        }

        //Rotation Icon
        rotationIcon.setOnClickListener {
            if (editedBitmap != null) {
                //ROTATION
                val matrix = Matrix()
                matrix.postRotate(90F)
                editedBitmap = Bitmap.createBitmap(editedBitmap!!, 0, 0, editedBitmap!!.width, editedBitmap!!.height, matrix, true)
            }
            else if (currentBitmap != null) {
                //ROTATION
                val matrix = Matrix()
                matrix.postRotate(90F)
                editedBitmap = Bitmap.createBitmap(currentBitmap!!, 0, 0, currentBitmap!!.width, currentBitmap!!.height, matrix, true)
            }

            val img = RoundedBitmapDrawableFactory.create(activity!!.resources, editedBitmap)
            img.cornerRadius = 250F
            userIcon.setPadding(50)

            //IMAGE UPDATE
            userIcon.setImageDrawable(img)
            viewModel.bitmapChanged = true
        }

        //Delete Image Icon
        deleteImageButton.setOnClickListener {
            resetBitmap()
        }

        //Camera Icon
        cameraIcon.setOnClickListener {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            try {
                startActivityForResult(takePictureIntent, 1)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(requireContext(), "Failed to access camera", Toast.LENGTH_SHORT).show()
            }
        }

        //Gallery Icon
        galleryIcon.setOnClickListener {
            val getPictureIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            try {
                startActivityForResult(getPictureIntent, 2)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(requireContext(), "Failed to access gallery", Toast.LENGTH_SHORT).show()
            }
        }

        //Edit Username Icon
        editUsernameIcon.setOnClickListener {
            //ICON ANIMATION
            YoYo.with(Techniques.Tada).duration(400).playOn(editUsernameIcon)

            //CUSTOM DIALOG
            val builder = AlertDialog.Builder(requireContext(), R.style.CustomDialog)
            val viewDialog = layoutInflater.inflate(R.layout.custom_update_username_dialog, null)
            builder.setView(viewDialog)
            builder.setCustomTitle(DialogCustomizer.getCustomizedDialogTitle("Username Update", requireContext()))
            builder.setNegativeButton("CANCEL", null)
            builder.setPositiveButton("UPDATE") { _, _ ->
                //ATTRIBUTES
                val usernameInput: EditText = viewDialog.findViewById(R.id.username_input)

                //INPUT ERROR CONTROL
                if (usernameInput.text.toString() == "")
                    Toast.makeText(requireContext(), "The username has not been updated",
                        Toast.LENGTH_SHORT).show()
                else {
                    username.text = usernameInput.text.toString()
                    newUsername = usernameInput.text.toString()
                }
            }
            builder.setCancelable(false)
            val dialog = builder.create()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //Set to Transparent to only see the custom bg.
            dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
            dialog.show()
        }

        //Change Password Button
        changePasswordButton.setOnClickListener {
            //CUSTOM DIALOG
            val builder = AlertDialog.Builder(requireContext(), R.style.CustomDialog)
            val viewDialog = layoutInflater.inflate(R.layout.custom_password_change_dialog, null)
            builder.setView(viewDialog)
            builder.setCustomTitle(DialogCustomizer.getCustomizedDialogTitle("Change Password", requireContext()))
            builder.setNegativeButton("CANCEL", null)
            builder.setPositiveButton("UPDATE") { _, _ ->
                //ATTRIBUTES
                val passwordInput: EditText = viewDialog.findViewById(R.id.password_input)

                //INPUT ERROR CONTROL
                if (Pattern.compile("[a-zA-Z0-9!@#$]{8,24}").matcher(passwordInput.text.toString()).matches()) {
                    FirebaseAuth.getInstance().currentUser!!.updatePassword(passwordInput.text.toString())
                    viewModel.password = passwordInput.text.toString()
                    Toast.makeText(requireContext(), "Password updated", Toast.LENGTH_SHORT).show()
                }
                else
                    Toast.makeText(requireContext(), "Invalid password", Toast.LENGTH_SHORT).show()

            }
            builder.setCancelable(false)
            val dialog = builder.create()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //Set to Transparent to only see the custom bg.
            dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
            dialog.show()
        }

        //Delete User Button
        deleteUserButton.setOnClickListener {
            //CUSTOM DIALOG
            val builder = AlertDialog.Builder(requireContext(), R.style.CustomDialog)
            val viewDialog = layoutInflater.inflate(R.layout.custom_delete_user_dialog, null)
            builder.setView(viewDialog)
            builder.setCustomTitle(DialogCustomizer.getCustomizedDialogTitle("Delete User", requireContext()))
            builder.setNegativeButton("CANCEL", null)
            builder.setPositiveButton("DELETE") { _, _ ->
                //ATTRIBUTES
                val emailInput: EditText = viewDialog.findViewById(R.id.email_input)

                //INPUT ERROR CONTROL
                if (emailInput.text.toString() != FirebaseAuth.getInstance().currentUser!!.email)
                    Toast.makeText(requireContext(), "Incorrect email", Toast.LENGTH_SHORT).show()
                else {
                    FirebaseAuth.getInstance().currentUser!!.delete()

                    //VIEW MODEL UPDATE
                    viewModel.editMode = false

                    //NAVIGATION
                    findNavController().navigate(R.id.action_user_to_login)
                }
            }
            builder.setCancelable(false)
            val dialog = builder.create()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //Set to Transparent to only see the custom bg.
            dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
            dialog.show()
        }

        //Save Changes Button
        saveChangesButton.setOnClickListener {
            setUpLayout(false)

            //VIEW MODEL UPDATE
            viewModel.editMode = false

            //USER UPDATES
            if (newUsername != "") {
                FirebaseAuth.getInstance().currentUser?.updateProfile(UserProfileChangeRequest.Builder().setDisplayName(newUsername).build())
                newUsername = ""
                viewModel.newUsername = ""
            }
            if (editedBitmap != null)
                CameraAndGalleryManagement.addProfileImageToStorage(editedBitmap)
            else
                FirebaseStorage.getInstance().reference.child("images/" + FirebaseAuth.getInstance().currentUser!!.email!! + "/profileIcon").delete()
        }

        //Cancel Changes Button
        cancelChangesButton.setOnClickListener {
            setUpLayout(false)
            resetBitmap()

            newUsername = ""

            //VIEW MODEL UPDATE
            viewModel.newUsername = ""
            viewModel.editMode = false

            //RESET USER DATA
            setUpUserData()
        }

        //Logout Text
        logoutText.setOnClickListener {
            goToLogin()
        }

        //Logout Icon
        logoutIcon.setOnClickListener {
            goToLogin()
        }
    }

    //METHODS
    /**
     * This method is used to set up the user data.
     */
    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    private fun setUpUserData() {
        //LOADING PANEL
        loadingPanel.visibility = View.VISIBLE
        var imageUri: Uri? = null

        FirebaseStorage.getInstance().getReferenceFromUrl("gs://geographically-341418.appspot.com/")
            .child("images/" + FirebaseAuth.getInstance().currentUser!!.email!! + "/profileIcon")
            .downloadUrl.addOnSuccessListener {
                imageUri = it
                Log.d("URI", imageUri.toString())
            }

        Handler(Looper.getMainLooper()).postDelayed({
            if (imageUri != null) {
                Glide.with(requireContext()).asBitmap().load(imageUri).into(object : CustomTarget<Bitmap>() {
                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                        val img = RoundedBitmapDrawableFactory.create(activity!!.resources, resource)
                        img.cornerRadius = 250F
                        userIcon.setPadding(50)
                        userIcon.setImageDrawable(img)

                        currentBitmap = resource
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                    }
                })
            }
            //LOADING PANEL
            loadingPanel.visibility = View.GONE
        }, 1000)

        username.text = FirebaseAuth.getInstance().currentUser?.displayName.toString()
        if (FirebaseAuth.getInstance().currentUser?.displayName == null
            || FirebaseAuth.getInstance().currentUser?.displayName == "")
            username.text = "Username"
        email.text = FirebaseAuth.getInstance().currentUser?.email
        joiningDate.text = "Joining date: ${SimpleDateFormat("dd/MM/yyyy").format(
            Date(FirebaseAuth.getInstance().currentUser?.metadata?.creationTimestamp!!))}"
    }

    /**
     * This method is used to navigate to the login screen.
     */
    private fun goToLogin() {
        //SIGN OUT
        FirebaseAuth.getInstance().signOut()

        //VIEW MODEL UPDATE
        viewModel.editMode = false

        //EXIT ANIMATION
        view?.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.fragment_exit_animation))

        //NAVIGATION
        findNavController().navigate(R.id.action_user_to_login)
    }

    /**
     * This method is used to set up the layout depending on the viewing mode.
     * @param editMode True if on edit mode, false otherwise
     */
    private fun setUpLayout(editMode: Boolean) {
        //LAYOUT UPDATE
        if (editMode) {
            editUserIcon.visibility = View.GONE
            rotationIcon.visibility = View.VISIBLE
            deleteImageButton.visibility = View.VISIBLE
            cameraIcon.visibility = View.VISIBLE
            galleryIcon.visibility = View.VISIBLE
            editUsernameIcon.visibility = View.VISIBLE
            changePasswordButton.visibility = View.VISIBLE
            deleteUserButton.visibility = View.VISIBLE
            saveChangesButton.visibility = View.VISIBLE
            cancelChangesButton.visibility = View.VISIBLE
            logoutText.visibility = View.GONE
            logoutIcon.visibility = View.GONE
        }
        else {
            editUserIcon.visibility = View.VISIBLE
            rotationIcon.visibility = View.GONE
            deleteImageButton.visibility = View.GONE
            cameraIcon.visibility = View.GONE
            galleryIcon.visibility = View.GONE
            editUsernameIcon.visibility = View.GONE
            changePasswordButton.visibility = View.GONE
            deleteUserButton.visibility = View.GONE
            saveChangesButton.visibility = View.GONE
            cancelChangesButton.visibility = View.GONE
            logoutText.visibility = View.VISIBLE
            logoutIcon.visibility = View.VISIBLE
        }
    }

    /**
     * This method is used to reset the bitmap to the original image.
     */
    private fun resetBitmap() {
        editedBitmap = null
        viewModel.bitmap = null
        viewModel.bitmapChanged = true
        userIcon.setPadding(0)
        userIcon.setImageResource(TypedValue().also { activity!!.theme.resolveAttribute(R.attr.user_icon, it, true) }.resourceId)
    }

    //ON ACTIVITY RESULT
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        editedBitmap = CameraAndGalleryManagement.getBitmapFromRequest(requestCode, resultCode, data, activity!!, userIcon, true)

        //VIEW MODEL UPDATE
        viewModel.bitmapChanged = true
    }

    //ORIENTATION CHANGE CONTROL

    //ON PAUSE
    override fun onPause() {
        super.onPause()
        //USERNAME PERSISTENCE
        if (newUsername != "")
            viewModel.newUsername = newUsername

        //IMAGE PERSISTENCE
        viewModel.bitmap = editedBitmap
    }

    //ON RESUME
    override fun onResume() {
        super.onResume()
        //USERNAME PERSISTENCE
        if (viewModel.editMode) {
            setUpLayout(true)

            if (viewModel.newUsername != "") {
                newUsername = viewModel.newUsername
                username.text = newUsername
            }
        }
    }

    //ON VIEW STATE RESTORED
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        //IMAGE PERSISTENCE
        if (viewModel.bitmapChanged) {
            if (viewModel.bitmap != null) {
                editedBitmap = viewModel.bitmap
                userIcon.setImageBitmap(editedBitmap)
            }
            else
                userIcon.setImageResource(R.drawable.default_image)
        }
    }
}
