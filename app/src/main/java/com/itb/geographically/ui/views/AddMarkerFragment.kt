package com.itb.geographically.ui.views

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Matrix
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.itb.geographically.R
import com.itb.geographically.data.models.MarkerGeo
import com.itb.geographically.ui.viewModel.GeographicallyViewModel
import com.itb.geographically.utils.CameraAndGalleryManagement
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.textfield.TextInputLayout
import java.util.*

class AddMarkerFragment : Fragment(R.layout.fragment_add_marker) {
    //ATTRIBUTES
    private lateinit var closingIcon: ImageView
    private lateinit var coordsLayout: TextInputLayout
    private lateinit var coordsInput: EditText
    private lateinit var goToMapIcon: ImageView
    private lateinit var arrow: ImageView
    private lateinit var goToMapIconTip: TextView
    private lateinit var nameLayout: TextInputLayout
    private lateinit var nameInput: EditText
    private lateinit var typeLayout: TextInputLayout
    private lateinit var typeInput: AutoCompleteTextView
    private lateinit var descriptionInput: EditText
    private lateinit var cameraIcon: ImageView
    private lateinit var galleryIcon: ImageView
    private lateinit var deleteImageIcon: ImageView
    private lateinit var image: ImageView
    private lateinit var rotationIcon: ImageView
    private lateinit var createButton: AppCompatButton

    //Bitmap
    private var bitmap: Bitmap? = null

    //View Model
    private val viewModel: GeographicallyViewModel by activityViewModels()

    //ON VIEW CREATED
    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //ENTER ANIMATION
        view.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.fragment_enter_animation))

        //VIEW MODEL UPDATE (Stop Animation)
        viewModel.stopAnimation = true

        //IDs
        closingIcon = view.findViewById(R.id.close_button)
        coordsLayout = view.findViewById(R.id.text_input_layout1)
        coordsInput = view.findViewById(R.id.coords_input)
        goToMapIcon = view.findViewById(R.id.go_to_map_icon)
        arrow = view.findViewById(R.id.arrow)
        goToMapIconTip = view.findViewById(R.id.map_icon_tip)
        nameLayout = view.findViewById(R.id.text_input_layout2)
        nameInput = view.findViewById(R.id.name_input)
        typeLayout = view.findViewById(R.id.text_input_layout3)
        typeInput = view.findViewById(R.id.type_input)
        descriptionInput = view.findViewById(R.id.description_input)
        cameraIcon = view.findViewById(R.id.camera_icon)
        galleryIcon = view.findViewById(R.id.gallery_icon)
        deleteImageIcon = view.findViewById(R.id.delete_image_button)
        image = view.findViewById(R.id.image)
        rotationIcon = view.findViewById(R.id.rotation_icon)
        createButton = view.findViewById(R.id.create_button)

        //COORDS INPUT SET UP
        coordsInput.setText("${arguments!!.getFloat("latitude")}, ${arguments!!.getFloat("longitude")}")

        //LAYOUT UPDATE (from Map vs from List + icon info)
        if (viewModel.currentFragment == "List") {
            goToMapIcon.visibility = View.VISIBLE
            if (viewModel.firstTimeAddFromList) {
                viewModel.firstTimeAddFromList = false
                arrow.visibility = View.VISIBLE
                goToMapIconTip.visibility = View.VISIBLE
            }
        }

        //TYPE INPUT SET UP
        typeInput.setAdapter(ArrayAdapter(requireContext(), R.layout.support_simple_spinner_dropdown_item, viewModel.typesOfLocation))

        //ON CLICK
        //Closing Icon
        closingIcon.setOnClickListener {
            //EXIT ANIMATION
            view.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.fragment_exit_animation))

            //NAVIGATION
            exitFragment(false)
        }

        //Go To Map Icon
        goToMapIcon.setOnClickListener {
            //NAVIGATION
            exitFragment(true)
        }

        //Camera Icon
        cameraIcon.setOnClickListener {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            try {
                startActivityForResult(takePictureIntent, 1)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(requireContext(), "Failed to access camera", Toast.LENGTH_SHORT).show()
            }
        }

        //Gallery Icon
        galleryIcon.setOnClickListener {
            val getPictureIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            try {
                startActivityForResult(getPictureIntent, 2)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(requireContext(), "Failed to access gallery", Toast.LENGTH_SHORT).show()
            }
        }

        //Delete Image Icon
        deleteImageIcon.setOnClickListener {
            //LAYOUT UPDATE
            setUpLayout(false)

            //BITMAP UPDATE
            bitmap = null
            viewModel.bitmap = null
        }

        //Rotation Icon
        rotationIcon.setOnClickListener {
            //ROTATION
            val matrix = Matrix()
            matrix.postRotate(90F)
            bitmap = Bitmap.createBitmap(bitmap!!, 0, 0, bitmap!!.width, bitmap!!.height, matrix, true)

            //IMAGEVIEW UPDATE
            image.setImageBitmap(bitmap)
        }

        //Create Button
        createButton.setOnClickListener {
            validateMarkerCreation()
        }
    }

    //METHODS
    /**
     * This method is used to validate the inputs when creating a new marker.
     */
    private fun validateMarkerCreation() {
        //ERROR CONTROL
        //No Name
        if (nameInput.text.toString() == "")
            nameLayout.error = "Name is required"
        //Name
        if (nameInput.text.toString() != "")
            nameLayout.error = null
        //No Type
        if (typeInput.text.toString() == "")
            typeLayout.error = "Type is required"
        //Type
        if (typeInput.text.toString() != "")
            typeLayout.error = null
        //Name + Type
        if (nameInput.text.toString() != "" && typeInput.text.toString() != "") {
            nameLayout.error = null
            typeLayout.error = null

            //NEW MARKER (Local Insert)
            val marker = MarkerGeo(viewModel.counter, nameInput.text.toString(), LatLng(arguments!!.getFloat("latitude").toDouble(),
                arguments!!.getFloat("longitude").toDouble()), typeInput.text.toString(), descriptionInput.text.toString(), bitmap, Calendar.getInstance().time)
            viewModel.markers.add(marker)

            //FIREBASE STORAGE for images
            if (marker.image != null)
                CameraAndGalleryManagement.addImageToStorage(marker)

            //FIRESTORE INSERT
            viewModel.insertMarkerIntoFirestore(marker)

            //VIEW MODEL UPDATE
            viewModel.counter++
            viewModel.bitmap = null

            //EXIT ANIMATION
            view?.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.fragment_exit_animation))

            //NAVIGATION
            exitFragment(false)
        }
    }

    /**
     * This method is used to set up the visibility of the views on the screen depending on the mode.
     * @param imageMode True if there is an image, false if there isn't one.
     */
    private fun setUpLayout(imageMode: Boolean) {
        //IMAGE
        if (imageMode) {
            rotationIcon.visibility = View.VISIBLE
            image.visibility = View.VISIBLE
            deleteImageIcon.visibility = View.VISIBLE
            cameraIcon.visibility = View.GONE
            galleryIcon.visibility = View.GONE
        }
        //CAMERA AND GALLERY ICONS
        else {
            rotationIcon.visibility = View.GONE
            image.visibility = View.GONE
            deleteImageIcon.visibility = View.GONE
            cameraIcon.visibility = View.VISIBLE
            galleryIcon.visibility = View.VISIBLE
        }
    }

    /**
     * This method is used to determine the fragment where the user will be navigating depending on the last fragment.
     * @param toMap Used to indicate if the navigation must be to the map or the last fragment.
     */
    private fun exitFragment(toMap: Boolean) {
        //TO MAP
        if (viewModel.currentFragment == "Map" || toMap)
            findNavController().navigate(R.id.action_addMarker_to_map)
        //TO LIST
        else if (viewModel.currentFragment == "List")
            findNavController().navigate(R.id.action_addMarker_to_markerList)
    }

    //ON ACTIVITY RESULT
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        bitmap = CameraAndGalleryManagement.getBitmapFromRequest(requestCode, resultCode, data, activity!!, image, false)

        if (bitmap != null)
            //LAYOUT UPDATE
            setUpLayout(true)
    }

    //ORIENTATION CHANGE CONTROL

    //ON PAUSE
    override fun onPause() {
        super.onPause()
        //IMAGE PERSISTENCE
        if (bitmap != null)
            viewModel.bitmap = bitmap
    }

    //ON RESUME
    override fun onResume() {
        super.onResume()
        //IMAGE PERSISTENCE
        if (viewModel.bitmap != null) {
            bitmap = viewModel.bitmap
            image.setImageBitmap(bitmap)
            setUpLayout(true)
        }
    }
}
