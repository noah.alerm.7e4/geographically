package com.itb.geographically.ui.views

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Patterns
import android.view.View
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.itb.geographically.R
import com.itb.geographically.ui.viewModel.GeographicallyViewModel
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class LoginFragment : Fragment(R.layout.fragment_login) {
    //ATTRIBUTES
    private lateinit var loadingPanel: RelativeLayout
    private lateinit var emailLayout: TextInputLayout
    private lateinit var emailInput: EditText
    private lateinit var passwordLayout: TextInputLayout
    private lateinit var passwordInput: EditText
    private lateinit var signInButton: Button
    private lateinit var googleSignInButton: AppCompatButton
    private lateinit var signUpText: TextView

    //View Model
    private val viewModel: GeographicallyViewModel by activityViewModels()

    //ON VIEW CREATED
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //IDs
        loadingPanel = view.findViewById(R.id.loading_panel)
        emailLayout = view.findViewById(R.id.text_input_layout1)
        emailInput = view.findViewById(R.id.email_input)
        passwordLayout = view.findViewById(R.id.text_input_layout2)
        passwordInput = view.findViewById(R.id.password_input)
        signInButton = view.findViewById(R.id.login_button)
        googleSignInButton = view.findViewById(R.id.google_login_button)
        signUpText = view.findViewById(R.id.sign_up_link)

        //INPUTS UPDATE (Mainly used after Registration)
        if (FirebaseAuth.getInstance().currentUser != null) {
            viewModel.email = FirebaseAuth.getInstance().currentUser!!.email

            //NAVIGATION (If the user is logged in, they can navigate directly to the map)
            exitToMap()
        }
        if (viewModel.email != "") {
            emailInput.setText(viewModel.email)
            viewModel.email = ""
        }
        if (viewModel.password != "") {
            passwordInput.setText(viewModel.password)
            viewModel.password = ""
        }

        //VIEW MODEL UPDATE
        viewModel.currentMapTheme = ""
        viewModel.firstTimeInList = true

        //ON CLICK
        //Password Input (If not used, the error icon stops the user from using the eye icon)
        passwordInput.setOnClickListener {
            passwordLayout.error = null
        }
        passwordInput.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus)
                passwordLayout.error = null
        }

        //Sign In Button
        signInButton.setOnClickListener {
            validateLoginEmail()
        }

        //Google Sign In Button
        googleSignInButton.setOnClickListener {
            validateLoginGoogle()
        }

        //Sign Up Text
        signUpText.setOnClickListener {
            //NAVIGATION
            findNavController().navigate(R.id.action_login_to_registration)
        }
    }

    //METHODS
    /**
     * This method is used to validate the login inputs.
     */
    @SuppressLint("SimpleDateFormat")
    private fun validateLoginEmail() {
        //ERROR CONTROL
        //Invalid Email
        if (!Patterns.EMAIL_ADDRESS.matcher(emailInput.text.toString()).matches())
            emailLayout.error = "Invalid email address"
        //No Email
        if (emailInput.text.toString() == "")
            emailLayout.error = "Email is required"
        //Valid Email (Used when previously no / invalid email and now no password)
        if (Patterns.EMAIL_ADDRESS.matcher(emailInput.text.toString()).matches())
            emailLayout.error = null
        //No Password
        if (passwordInput.text.toString() == "")
            passwordLayout.error = "Password is required"
        //Password (Used when no password previously and now no email or invalid)
        if (passwordInput.text.toString() != "")
            passwordLayout.error = null
        //Valid Email + Password
        if (Patterns.EMAIL_ADDRESS.matcher(emailInput.text.toString()).matches() && passwordInput.text.toString() != "") {
            emailLayout.error = null
            passwordLayout.error = null

            //FIREBASE LOGIN EMAIL
            FirebaseAuth.getInstance().
            signInWithEmailAndPassword(emailInput.text.toString(), passwordInput.text.toString())
                .addOnCompleteListener { task ->
                    if(task.isSuccessful){
                        //USER DATA UPDATE (ViewModel)
                        viewModel.email = task.result?.user?.email
                        viewModel.password = passwordInput.text.toString()

                        exitToMap()
                    }
                    else
                        Toast.makeText(requireContext(), "The email and/or the password are incorrect", Toast.LENGTH_SHORT).show()

                }
        }
    }

    /**
     * This method is used to sign in through Goggle.
     */
    private fun validateLoginGoogle() {
        //GOOGLE SIGN IN OPTIONS
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        //INTENT
        val client = GoogleSignIn.getClient(activity!!, gso)
        client.signOut()
        startActivityForResult(client.signInIntent, 1)
    }

    /**
     * This method is used to exit from the login to the map fragment.
     */
    private fun exitToMap() {
        //LOADING PANEL
        loadingPanel.visibility = View.VISIBLE

        //USER MARKER GETTER (Used when the user logs in)
        viewModel.getMarkersFromFirestore()

        //NAVIGATION
        findNavController().navigate(R.id.action_login_to_map)
    }

    //ON ACTIVITY RESULT
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        //FIREBASE LOGIN GOOGLE
        if (requestCode == 1) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                //SUCCESSFUL
                val account = task.getResult(ApiException::class.java)

                val credential = GoogleAuthProvider.getCredential(account.idToken, null)
                Firebase.auth.signInWithCredential(credential).addOnCompleteListener {
                        if (it.isSuccessful) {
                            //USER DATA UPDATE (ViewModel)
                            viewModel.email = Firebase.auth.currentUser!!.email

                            //FIRESTORE INSERT
                            Firebase.firestore.collection("users").document(it.result?.user?.email!!).get().addOnCompleteListener { docTask ->
                                if (docTask.isSuccessful){
                                    if(!docTask.result.exists())
                                        Firebase.firestore.collection("users").document(it.result?.user?.email!!).set(hashMapOf("mapStyle" to "Standard"))
                                }
                            }

                            Handler(Looper.getMainLooper()).postDelayed({
                                //LOADING PANEL
                                loadingPanel.visibility = View.VISIBLE

                                exitToMap()
                            }, 500)
                        } else
                            Toast.makeText(requireContext(), "An error has occurred during the login", Toast.LENGTH_SHORT).show()

                }
            } catch (e: ApiException) {
                //FAILED
                Toast.makeText(requireContext(), "An error has occurred during the login", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
