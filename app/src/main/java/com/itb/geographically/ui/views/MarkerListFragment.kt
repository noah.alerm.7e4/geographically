package com.itb.geographically.ui.views

import android.annotation.SuppressLint
import android.content.res.Configuration
import android.graphics.Canvas
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.itb.geographically.R
import com.itb.geographically.data.models.MarkerGeo
import com.itb.geographically.ui.adapters.ListAdapter
import com.itb.geographically.ui.viewModel.GeographicallyViewModel
import com.itb.geographically.utils.CameraAndGalleryManagement
import com.itb.geographically.utils.NavigationDrawerOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator


class MarkerListFragment : Fragment(R.layout.fragment_marker_list) {
    //ATTRIBUTES
    private lateinit var loadingPanel: RelativeLayout
    private lateinit var menuIcon: ImageView
    private lateinit var searchView: SearchView
    private lateinit var recyclerView: RecyclerView
    private lateinit var addButton: FloatingActionButton

    //Navigation Drawer
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navigationView: NavigationView

    //View Model
    private val viewModel: GeographicallyViewModel by activityViewModels()

    //Refresh
    private lateinit var swipeRefresh: SwipeRefreshLayout

    //ON VIEW CREATED
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //IDs
        loadingPanel = view.findViewById(R.id.loading_panel)
        menuIcon = view.findViewById(R.id.menu_icon)
        searchView = view.findViewById(R.id.search_view)
        drawerLayout = activity!!.findViewById(R.id.drawerLayout)
        navigationView = activity!!.findViewById(R.id.navigationView)
        swipeRefresh = view.findViewById(R.id.refresh)
        recyclerView = view.findViewById(R.id.recycler_view)
        addButton = view.findViewById(R.id.add_button)

        //CURRENT FRAGMENT UPDATE (Used when the user comes back from the User Fragment)
        if (arguments!!.getBoolean("isFavorites")) {
            viewModel.currentFragment = "Favorites"
            addButton.visibility = View.GONE
        }
        else
            viewModel.currentFragment = "List"

        //ENTER ANIMATION
        if (!viewModel.stopAnimation)
            view.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.fragment_enter_animation))
        else
            viewModel.stopAnimation = false

        //REFRESH
        if (viewModel.currentFragment == "Favorites") {
            swipeRefresh.setOnRefreshListener {
                viewModel.stopAnimation = true

                //NAVIGATION
                findNavController().navigate(R.id.action_markerList_self, bundleOf("isFavorites" to true))
            }
        }
        else
            swipeRefresh.isEnabled = false

        //RECYCLER VIEW
        recyclerView.layoutManager = LinearLayoutManager(requireContext()) //Vertical Layout
        if (activity!!.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
            recyclerView.layoutManager = GridLayoutManager(requireContext(), 2) //Horizontal Layout
        setUpNormalAdapter()

        //ON CLICK
        //Menu Icon
        menuIcon.setOnClickListener {
            NavigationDrawerOptions.manageNavigationOptions(drawerLayout, navigationView, activity!!, viewModel)
        }

        //Add Button
        addButton.setOnClickListener {
            //NAVIGATION
            if (viewModel.currentPosition != null) {
                val action = MarkerListFragmentDirections.actionMarkerListToAddMarker(viewModel.currentPosition!!.latitude.toFloat(),
                    viewModel.currentPosition!!.longitude.toFloat())
                findNavController().navigate(action)
            }
            else
                Toast.makeText(requireContext(), "Make sure you have location permissions enabled", Toast.LENGTH_SHORT).show()
        }

        //SEARCH
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            //TEXT SUBMIT
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            //TEXT CHANGE
            override fun onQueryTextChange(newText: String?): Boolean {
                setUpFilteredAdapter(newText!!)
                return false
            }
        })
    }

    //METHODS
    /**
     * This method is used to set up the recycler view's adapter without any filters.
     */
    private fun setUpNormalAdapter() {
        //Favorites (no deleting)
        if (arguments!!.getBoolean("isFavorites"))
            recyclerView.adapter = ListAdapter(viewModel.markers.filter { it.isFavorite }, loadingPanel, viewModel)
        //List (with marker deletion)
        else {
            recyclerView.adapter = ListAdapter(viewModel.markers, loadingPanel, viewModel)

            //RECYCLER VIEW MANAGEMENT
            ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
                //MOVE
                override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target:
                RecyclerView.ViewHolder): Boolean = false

                //SWIPE
                @SuppressLint("NotifyDataSetChanged")
                override fun onSwiped(h: RecyclerView.ViewHolder, dir: Int) {
                    val position = h.bindingAdapterPosition
                    val item = viewModel.markers[position]

                    //LOCAL DELETE
                    viewModel.markers.removeAt(position)

                    //FIRESTORE DELETE
                    Firebase.firestore.collection("users").document(FirebaseAuth.getInstance().currentUser!!.email!!)
                        .collection("markers").document(item.id.toString()).delete()

                    //STORAGE IMAGE DELETE
                    FirebaseStorage.getInstance().reference.child("images/" + FirebaseAuth.getInstance().currentUser!!.email!! + "/" + item.id).delete()

                    //Recycler view update
                    recyclerView.adapter = ListAdapter(viewModel.markers, loadingPanel, viewModel)

                    //SNACKBAR (UNDO)
                    val snackbar = Snackbar.make(recyclerView, "${item.name} was removed from the list", Snackbar.LENGTH_LONG)
                    snackbar.setAction("UNDO") {
                        //NEW MARKER
                        val undoneMarker = MarkerGeo(item.id, item.name, item.coords, item.type, item.description, item.image, item.creationDate, item.tags, item.isFavorite)

                        //LOCAL UPDATE / INSERT
                        viewModel.markers.add(undoneMarker)

                        //FIRESTORE INSERT
                        viewModel.insertMarkerIntoFirestore(undoneMarker)

                        //STORAGE IMAGE INSERT
                        if (item.image != null)
                            CameraAndGalleryManagement.addImageToStorage(item)

                        viewModel.stopAnimation = true

                        //NAVIGATION
                        findNavController().navigate(R.id.action_markerList_self, bundleOf("isFavorites" to false))
                        recyclerView.scrollToPosition(position)
                    }
                    snackbar.show()
                }

                //CHILD DRAW
                override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
                                         dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
                    super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

                    //RED BACKGROUND
                    var bg = ContextCompat.getDrawable(requireContext(), R.drawable.swipe_to_delete_bg)
                    bg?.bounds = Rect(
                        viewHolder.itemView.right + dX.toInt() - 100, viewHolder.itemView.top,
                        viewHolder.itemView.right, viewHolder.itemView.bottom
                    )
                    bg?.draw(c)

                    //NORMAL BACKGROUND
                    //We need to reset the background due to the view's rounded corners.
                    if (dX == 0F) {
                        bg = ContextCompat.getDrawable(requireContext(), R.drawable.normal_bg)
                        bg?.bounds = Rect(
                            viewHolder.itemView.right + dX.toInt() - 100, viewHolder.itemView.top,
                            viewHolder.itemView.right, viewHolder.itemView.bottom
                        )
                        bg?.draw(c)
                    }

                    //SWIPE DECORATOR (see Gradle Implementation)
                    //Used to add an icon
                    RecyclerViewSwipeDecorator.Builder(requireContext(), c, recyclerView, viewHolder, dX,
                        dY, actionState, isCurrentlyActive).addActionIcon(R.drawable.trash_can).create().decorate()
                }
            }).attachToRecyclerView(recyclerView)
        }
    }

    /**
     * This method is used to set up the recycler view's adapter with filters (SEARCH).
     */
    private fun setUpFilteredAdapter(text: String) {
        //List
        if (viewModel.currentFragment == "List")
            recyclerView.adapter = ListAdapter(viewModel.markers.filter { it.name.lowercase().contains(text.lowercase())
                    || it.type.lowercase().contains(text.lowercase()) || it.tags.contains(text) },
                loadingPanel, viewModel)
        //Favorites
        else
            recyclerView.adapter = ListAdapter(viewModel.markers.filter { (it.name.lowercase().contains(text.lowercase())
                    || it.type.lowercase().contains(text.lowercase()) || it.tags.contains(text))
                    && it.isFavorite }, loadingPanel, viewModel)
    }

    //ON STOP
    override fun onStop() {
        super.onStop()
        //VIEW MODEL UPDATE (If not done here, the bitmap is reset and the image is preserved when it shouldn't)
        viewModel.bitmap = null
        viewModel.firstTimeInList = false
    }
}
